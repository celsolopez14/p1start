package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {


    @Override
    public int compare(Car car1, Car car2) {
        int result = 0;
        result = car1.getCarBrand().compareTo(car2.getCarBrand());

        if (result != 0) {
            return result;
        }
        else
            result  = car1.getCarModel().compareTo(car2.getCarModel());
        if(result!=0){
            return result;
        }
        else
           return result = car1.getCarModelOption().compareTo(car2.getCarModelOption());

    }
}