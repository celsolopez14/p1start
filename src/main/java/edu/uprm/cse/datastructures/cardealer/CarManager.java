package edu.uprm.cse.datastructures.cardealer;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//It creates a path  at which the resource is available to receive HTTP requests.
@Path("/cars")
public class CarManager {
private final SortedList<Car> carList = CarList.getInstance();

//Here it creates and return an Array with all the cars at the CarList.
@GET
@Path("")
@Produces(MediaType.APPLICATION_JSON)
    public Car[] getAllCars() {
    Car[] arrayCar = new Car[carList.size()];
    for (int i = 0; i < carList.size(); i++) {
        arrayCar[i] = carList.get(i);
    }
    return arrayCar;
}
//It creates a path where it returns the Car that contains that id in the CarList.
@GET
@Path("/{id}")
@Produces(MediaType.APPLICATION_JSON)
    public Car getCar(@PathParam("id") long id){
    for (int i = 0; i <carList.size() ; i++) {
        if(carList.get(i).getCarId()==id){
            return carList.get(i);
        }
    }
    throw new NotFoundException(new JsonError("Error", "Cars" + id + "not found"));
}

//It creates a path where it adds the Car on the CarList.
@POST
@Path("/add")
@Produces(MediaType.APPLICATION_JSON)
    public Response addCar(Car car){
    carList.add(car);
    return Response.status(201).build();
}
//It creates a path where using the id and update, it changes the information of that car you want on the CarList.
    @PUT
    @Path("{id}/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCar(Car car){
        int matchIdx=0;
        for (int i = 0; i <carList.size() ; i++) {
            if(carList.get(i).getCarId()==car.getCarId()){
                carList.remove(i);
                carList.add(car);
                return Response.status(Response.Status.OK).build();
            }

        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }
//It creates a path where usign id and delete, it removes the Car from the CarList.
    @DELETE
    @Path("{id}/delete")
    public Response deleteCar(@PathParam("id") long id){
        for (int i = 0; i <carList.size() ; i++) {
            if(carList.get(i).getCarId()==id){
                carList.remove(i);
                return Response.status(Response.Status.OK).build();
            }

        }
        throw new NotFoundException(new JsonError("Error", "Car" + id + "not found"));
    }




}
