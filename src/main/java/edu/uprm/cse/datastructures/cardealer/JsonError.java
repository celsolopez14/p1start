package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.core.Response;

public class JsonError {
    private String type;
    private String message;

    public String getType() {
        return this.type;
    }

    public String getMessage() {
        return this.message;
    }


    public JsonError(String type, String message) {
        this.type = type;
        this.message = message;
    }
}
