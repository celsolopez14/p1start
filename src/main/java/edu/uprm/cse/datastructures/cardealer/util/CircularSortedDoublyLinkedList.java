package edu.uprm.cse.datastructures.cardealer.util;


import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {

    private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
        private Node<E> nextNode;

        public CircularSortedDoublyLinkedListIterator(){this.nextNode = (Node<E>) header.getNext();}

        @Override
        public boolean hasNext() {
            return nextNode != header;
        }

        public E next(){
            if(this.hasNext()){
                E result = this.nextNode.getElement();
                this.nextNode = this.nextNode.getNext();
                return result;
            }
            else{
                throw new NoSuchElementException();
            }
        }
    }

    private final Comparator<E> comparator;
    private static class Node<E>{
    private E element;
    private Node<E> next;
    private Node<E> prev;

    public Node(){
        this.element = null;
        this.next = this.prev=null;
    }

    public E getElement() {
        return element;
    }

    public void setElement(E element) {
        this.element = element;
    }

    public Node<E> getNext() {
        return next;
    }

    public void setNext(Node<E> next) {
        this.next = next;
    }

    public Node<E> getPrev() {
        return prev;
    }

    public void setPrev(Node<E> prev) {
        this.prev = prev;
    }
}

private Node<E> header;
private int currentSize;

public CircularSortedDoublyLinkedList(Comparator<E> comparator){
    this.currentSize=0;
    this.header = new Node<>();
    this.comparator = comparator;

    this.header.setNext(this.header);
    this.header.setPrev(this.header);
}


//this method returns true when an object is added to the list. Adds objects in increasing order.
    @Override
    public boolean add(E obj) {
    int result=0;
    if(this.isEmpty()){
        Node<E> newNode = new Node<E>();
        newNode.setElement(obj);
        newNode.setNext(this.header);
        newNode.setPrev(this.header.getPrev());
        this.header.setPrev(newNode);
        newNode.getPrev().setNext(newNode);
        currentSize++;
        return true;
    }
        for (Node temp = this.header.getNext(); temp!=this.header ; temp=temp.getNext()) {
            result = this.comparator.compare(obj, (E) temp.getElement());
            if(result<0) {
                Node<E> newNode = new Node<E>();
                newNode.setElement(obj);
                newNode.setNext(temp);
                newNode.setPrev(temp.getPrev());
                temp.setPrev(newNode);
                newNode.getPrev().setNext(newNode);
                currentSize++;
                return true;
            }

        }
    Node<E> newNode = new Node<E>();
    newNode.setElement(obj);
    newNode.setNext(this.header);
    newNode.setPrev(this.header.getPrev());
    this.header.setPrev(newNode);
    newNode.getPrev().setNext(newNode);
    currentSize++;
        return true;
    }
//This method returns the current size of the list.
    @Override
    public int size() {
        return this.currentSize;
    }

    @Override
    public boolean remove(E obj) {
    Node<E> temp = this.header.getNext();
    Node<E> target = null;
    if(this.firstIndex(obj)!=-1){
        target = temp.getNext();
        temp.setNext(target.getNext());
        target.setElement(null);
        target.setNext(null);
        this.currentSize--;
        return true;

    }

        return false;
    }
//It returns true when an object is removed from the list.
    @Override
    public boolean remove(int index) {
    if(index<0 || index>=this.currentSize){
        throw new IndexOutOfBoundsException();
    }
    if(isEmpty()){return false;}
        Node<E> temp = this.header;
        Node<E> target = null;
        int currentPosition =0;

        while(currentPosition!=index){
            temp = temp.getNext();
            currentPosition++;
        }
        target = temp.getNext();
        temp.setNext(target.getNext());
        target.getNext().setPrev(temp);
        target.setElement(null);
        target.setNext(null);
        target.setPrev(null);
        this.currentSize--;
        return true;

    }
//Returns the number of copies removed from the list of that object.
    @Override
    public int removeAll(E obj) {
    int result=0;
    while(this.contains(obj)){
        remove(this.firstIndex(obj));
        result++;
    }

    return result;
    }
//Returns the first object on the list.
    @Override
    public E first() {
        return this.header.getNext().getElement();
    }
//Returns the last object on the list.
    @Override
    public E last() {
        return this.header.getPrev().getElement();
    }
//Returns the object in that index on the list. (in that position)
    @Override
    public E get(int index) {
        if(this.isEmpty()){
            return null;
        }
        Node<E> temp = this.header.getNext();
        int currentPosition=0;
        while(currentPosition!=index){
            temp = temp.getNext();
            currentPosition++;
        }
        return temp.getElement();

    }
//This method removes every object on the list, making it a empty list.
    @Override
    public void clear() {
    while(!this.isEmpty()){
        remove(0);
    }

    }
//Returns true if the list contains that object.
    @Override
    public boolean contains(E e) {
    if(this.isEmpty()){return false;}
        for(Node temp = this.header.getNext(); temp!=header; temp = temp.getNext()){
            if(temp.getElement().equals(e)){
                return true;
            }
        }
        return false;
    }
//Returns true if the list is empty, if the size of it is 0.
    @Override
    public boolean isEmpty() {
        return this.size()==0;
    }

    @Override
    public int firstIndex(E e) {
        if (this.isEmpty() || !this.contains(e)) {
            return -1;
        }
        Node<E> temp = this.header.getNext();
        int currentPosition = 0;
            while (!temp.getElement().equals(e)) {
                temp = temp.getNext();
                currentPosition++;

        }
        return currentPosition;
    }
//Returns the last index(position) of that object on the list.
    @Override
    public int lastIndex(E e) {
        Node<E> temp = this.header.getPrev();
        int currentPosition=this.size()-1;
        if(this.contains(e)){
            while(!temp.getElement().equals(e)){
                temp = temp.getPrev();
                currentPosition--;
            }
            return currentPosition;
        }

        return -1;
    }

    @Override
    public Iterator<E> iterator() {
        return new CircularSortedDoublyLinkedListIterator<E>();
    }
}
